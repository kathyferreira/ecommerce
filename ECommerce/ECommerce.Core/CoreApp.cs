﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;

namespace ECommerce.Core
{
    /// <summary>
    /// Main application
    /// </summary>
    public class CoreApp : MvxApplication
    {
        //// <summary>
        /// Initialize the services and defines the first viewmodel
        /// </summary>
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            RegisterNavigationServiceAppStart<ViewModels.RootViewModel>();
        }
    }
}
