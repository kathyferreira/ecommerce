﻿using ECommerce.Core.Interfaces;
using MvvmCross.Platform;
using Plugin.Fingerprint.Abstractions;
using System.Threading.Tasks;

namespace ECommerce.Core.Services
{
    /// <summary>
    /// Fingerprint service access methods
    /// </summary>
    public class FingerprintService : IFingerprintService
    {
        private readonly IFingerprint fingerprint;

        /// <summary>
        /// Constructor
        /// </summary>
        public FingerprintService()
        {
            this.fingerprint = Mvx.Resolve<IFingerprint>();
        }

        /// <summary>
        /// Requests the authentication
        /// </summary>
        /// <returns>Authentication result</returns>
        public async Task<bool> AuthenticateFingerprintAsync()
        {
            if (await IsFingerprintAvailableAsync())
            {
                AuthenticationRequestConfiguration authRequestConfiguration = new AuthenticationRequestConfiguration("Coloque su huella dactilar")
                {
                    AllowAlternativeAuthentication = false,
                    CancelTitle = "Cancelar",
                    UseDialog = true
                };

                var result = await fingerprint.AuthenticateAsync(authRequestConfiguration);

                return result.Authenticated;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the Fingerprint is available in the phone
        /// </summary>
        /// <returns>True is Available</returns>
        public async Task<bool> IsFingerprintAvailableAsync()
        {
            return await fingerprint.IsAvailableAsync(true);
        }
    }
}
