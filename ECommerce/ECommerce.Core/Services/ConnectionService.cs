﻿using ECommerce.Core.Interfaces;
using Plugin.Connectivity;

namespace ECommerce.Core.Services
{
    /// <summary>
    /// Connection service loginc
    /// </summary>
    public class ConnectionService : IConnectionService
    {
        /// <summary>
        /// Check if the device is connected
        /// </summary>
        /// <returns>true if connected</returns>
        public bool CheckOnline()
        {
            return CrossConnectivity.Current.IsConnected;
        }
    }
}
