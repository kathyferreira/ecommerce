﻿using System.Threading.Tasks;

namespace ECommerce.Core.Interfaces
{
    /// <summary>
    /// Interface for the Fingerprint service
    /// </summary>
    public interface IFingerprintService
    {
        /// <summary>
        /// Requests the authentication
        /// </summary>
        /// <returns>Authentication result</returns>
        Task<bool> AuthenticateFingerprintAsync();
    }
}
