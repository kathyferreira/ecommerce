﻿namespace ECommerce.Core.Interfaces
{
    /// <summary>
    /// Interface for the connection service
    /// </summary>
    public interface IConnectionService
    {
        bool CheckOnline();
    }
}
