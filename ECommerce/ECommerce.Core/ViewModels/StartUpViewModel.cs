﻿namespace ECommerce.Core.ViewModels
{
    /// <summary>
    /// Root empty ViewModel for the AppCompact Activity
    /// </summary>
    public class StartUpViewModel : BaseViewModel
    {
        public StartUpViewModel()
        {
            ShowViewModel<RootViewModel>();
        }
    }
}
