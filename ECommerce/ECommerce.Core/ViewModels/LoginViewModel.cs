﻿using MvvmCross.Core.ViewModels;
using System;
using System.Threading.Tasks;

namespace ECommerce.Core.ViewModels
{
    /// <summary>
    /// Login logic
    /// </summary>
    public class LoginViewModel : BaseViewModel
    {
        /// <summary>
        /// Request the login
        /// </summary>
        public IMvxCommand LoginUser
        {
            get
            {
                return new MvxCommand(async () =>
                {
                    try
                    {
                        this.IsBusy = true;
                        // TODO: Login when button pressed

                    }
                    catch (Exception ex)
                    {
                        await App.Current.MainPage.DisplayAlert("Error", "Ocurrio un error: " + ex.Message, "OK");
                    }
                    finally
                    {
                        this.IsBusy = false;
                    }
                });
            }
        }

        /// <summary>
        /// Initializes the data for this ViewModel
        /// </summary>
        public override Task Initialize()
        {
            return Task.Run(async () =>
            {
                

            });
        }

        /// <summary>
        /// 
        /// </summary>
        public override async void Start()
        {
            base.Start();
            try
            {
                this.IsBusy = true;

                if (await this.FingerprintService.AuthenticateFingerprintAsync())
                {
                    await ShowMenuViewModel();
                    await ShowFirstViewModel();
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Ocurrio un error al tratar de leer su huella ", "OK");
                    // Si esto sucede debería haber un botón de huella para volver a intentar
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            finally
            {
                this.IsBusy = false;
            }
        }

        /// <summary>
        /// Request the navigation drawer menu
        /// </summary>
        /// <returns></returns>
        private async Task ShowMenuViewModel()
        {
            await this.NavigationService.Navigate<MenuViewModel>();
        }

        /// <summary>
        /// Shows the first view model
        /// </summary>
        /// <returns></returns>
        private async Task ShowFirstViewModel()
        {
            await this.NavigationService.Navigate<MainViewModel>();
        }
    }
}
