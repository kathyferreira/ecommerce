﻿using System.Threading.Tasks;

namespace ECommerce.Core.ViewModels
{
    /// <summary>
    /// Main ViewModel of the app
    /// </summary>
    public class MainViewModel : BaseViewModel
    {
        /// <summary>
        /// Initializes the data
        /// </summary>
        public override void Prepare()
        {

        }

        /// <summary>
        /// Initializes the data for this ViewModel
        /// </summary>
        /// <returns></returns>
        public override Task Initialize()
        {
            return Task.Run(async () =>
            {

            });
        }
    }
}
