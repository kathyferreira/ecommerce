﻿using ECommerce.Core.Interfaces;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using System;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace ECommerce.Core.ViewModels
{
    /// <summary>
    /// Base class for all our ViewModels
    /// </summary>
    public abstract class BaseViewModel : MvxViewModel
    {
        private bool isBusy;

        /// <summary>
        /// Get's if the viewModel is busy doing something
        /// </summary>
        public bool IsBusy
        {
            get => this.isBusy;
            set
            {
                this.isBusy = value;
                RaisePropertyChanged(() => IsBusy);
                RaisePropertyChanged(() => IsEnabled);
            }
        }

        /// <summary>
        /// Gets if the ViewModel is enabled.
        /// It's the inverse of IsBusy for easier binding. (If IsBusy = true them IsEnabled = false)
        /// </summary>
        public bool IsEnabled
        {
            get => !this.isBusy;
            set
            {
                this.isBusy = !value;
                RaisePropertyChanged(() => IsEnabled);
            }
        }

        /// <summary>
        /// Gets the current NavigationService
        /// </summary>
        protected IMvxNavigationService NavigationService
        {
            get;
        }

        /// <summary>
        /// Gets the current data service
        /// </summary>
        protected IDataService DataService
        {
            get;
        }

        /// <summary>
        /// Gets the current connection service
        /// </summary>
        protected IConnectionService ConnectionService
        {
            get;
        }

        /// <summary>
        /// Gets the current fingerprint service
        /// </summary>
        protected IFingerprintService FingerprintService
        {
            get;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        protected BaseViewModel()
        {
            this.DataService = Mvx.GetSingleton<IDataService>();
            this.ConnectionService = Mvx.GetSingleton<IConnectionService>();
            this.NavigationService = Mvx.GetSingleton<IMvxNavigationService>();
            this.FingerprintService = Mvx.GetSingleton<IFingerprintService>();
        }

        /// <summary>
        /// Checks if the Internet connection is available
        /// </summary>
        /// <returns></returns>
        public static bool IsInternetConnectionAvailable()
        {
            return NetworkInterface.GetIsNetworkAvailable();
        }

        /// <summary>
        /// Logs an exception and shows a message to the user
        /// </summary>
        public void LogException(Exception ex, string title = "ApplicationError", string message = "ApplicationErrorMessage")
        {
            //this.DataService.LogException(ex);
            //InvokeOnMainThread(() =>
            //{
            //    var platformService = Mvx.GetSingleton<IPlatformService>();
            //    platformService.NotifyAsync(GetText(title), GetText(message));
            //});

        }
    }

    /// <summary>
    /// Base ViewModel with parameters
    /// </summary>
    public abstract class BaseViewModel<TParameter> : BaseViewModel, IMvxViewModel<TParameter> where TParameter : class
    {
        /// <summary>
        /// Called with the ViewModel's parameters before Initialize()
        /// </summary>
        public abstract void Prepare(TParameter parameter);
    }

    /// <summary>
    /// Base ViewModel with parameters and result
    /// </summary>
    public abstract class BaseViewModel<TParameter, TResult> : BaseViewModel, IMvxViewModel<TParameter, TResult>
        where TParameter : class
        where TResult : class
    {
        //private TaskCompletionSource<TResult> taskCompletionSource;
        //private bool isClosing;

        public TaskCompletionSource<object> CloseCompletionSource { get; set; }

        /// <summary>
        /// Called with the ViewModel's parameters before Initialize()
        /// </summary>
        public abstract void Prepare(TParameter parameter);
    }
}
