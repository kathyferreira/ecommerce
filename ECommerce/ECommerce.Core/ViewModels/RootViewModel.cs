﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System.Threading.Tasks;

namespace ECommerce.Core.ViewModels
{
    /// <summary>
    /// ViewModel that acts a container for the navigation drawer
    /// </summary>
    public class RootViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService navigationService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="navigationService"></param>
        public RootViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        /// <summary>
        /// Initialize the view
        /// </summary>
        public override void ViewAppeared()
        {
            MvxNotifyTask.Create(async () =>
            {
                // TODO: Verifique si el user está logeado o no, para saber si dirigir a Login o Main, o siempre pedir login al iniciar app
                await ShowLoginViewModel();

                //await ShowMenuViewModel();
                //await ShowFirstViewModel();
            });
        }

        /// <summary>
        /// Shows the login view model
        /// </summary>
        /// <returns></returns>
        private async Task ShowLoginViewModel()
        {
            await this.navigationService.Navigate<LoginViewModel>();
        }

        /// <summary>
        /// Request the navigation drawer menu
        /// </summary>
        /// <returns></returns>
        private async Task ShowMenuViewModel()
        {
            await this.navigationService.Navigate<MenuViewModel>();
        }

        /// <summary>
        /// Shows the first view model
        /// </summary>
        /// <returns></returns>
        private async Task ShowFirstViewModel()
        {
            await this.navigationService.Navigate<MainViewModel>();
        }
    }
}
