﻿using MvvmCross.Forms.Views.Attributes;
using Xamarin.Forms.Xaml;

namespace ECommerce.Core.Pages
{
    /// <summary>
    /// Main page UI
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxMasterDetailPagePresentation]
    public partial class MainPage
	{
        /// <summary>
        /// Initializes the UI
        /// </summary>
		public MainPage ()
		{
			InitializeComponent ();
		}
	}
}