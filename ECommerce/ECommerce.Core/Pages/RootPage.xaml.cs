﻿using MvvmCross.Forms.Views.Attributes;
using Xamarin.Forms.Xaml;

namespace ECommerce.Core.Pages
{
    /// <summary>
    /// Master detail root page
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxMasterDetailPagePresentation(MasterDetailPosition.Root, WrapInNavigationPage = false)]
    public partial class RootPage
	{
        /// <summary>
        /// Constructor
        /// Initializes the UI
        /// </summary>
		public RootPage ()
		{
			InitializeComponent ();
		}
	}
}