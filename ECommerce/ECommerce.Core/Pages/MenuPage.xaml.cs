﻿using MvvmCross.Forms.Views.Attributes;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ECommerce.Core.Pages
{
    /// <summary>
    /// Navigation drawer menu page
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxMasterDetailPagePresentation(MasterDetailPosition.Master)]
    public partial class MenuPage
	{
        /// <summary>
        /// Constructor
        /// Initializes the UI
        /// </summary>
        public MenuPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Hides the master page when an item is clicked
        /// </summary>
        public void ToggleClicked(object sender, EventArgs e)
        {
            if (Parent is MasterDetailPage md)
            {
                md.MasterBehavior = MasterBehavior.Popover;
                md.IsPresented = !md.IsPresented;
            }
        }
    }
}