﻿using MvvmCross.Forms.Views.Attributes;
using Xamarin.Forms.Xaml;

namespace ECommerce.Core.Pages
{
    /// <summary>
    /// Login page UI
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxContentPagePresentation(WrapInNavigationPage = false)]
    public partial class LoginPage
	{
        /// <summary>
        /// Initializes the UI
        /// </summary>
		public LoginPage ()
		{
			InitializeComponent ();
		}
	}
}