﻿using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;
using Plugin.Fingerprint.Dialog;

namespace ECommerce.Droid.Dialogs
{
    /// <summary>
    /// Custom Fingerprint dialog
    /// </summary>
    public class CustomFingerprintDialogFragment : FingerprintDialogFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            // Removes fallback button
            view.FindViewById<Button>(Resource.Id.fingerprint_btnFallback).Visibility = ViewStates.Gone;

            // Sets title font color
            view.FindViewById<TextView>(Resource.Id.fingerprint_txtReason).SetTextColor(Color.Magenta);

            // Sets cancel button font color
            view.FindViewById<TextView>(Resource.Id.fingerprint_btnCancel).SetTextColor(Color.Magenta);

            // Sets background color
            view.Background = new ColorDrawable(Color.White);

            // The fingerprint image is set to black
            var image = view.FindViewById<ImageView>(Resource.Id.fingerprint_imgFingerprint);
            image.SetColorFilter(Color.ParseColor("#000000")); // black 

            return view;
        }
    }
}