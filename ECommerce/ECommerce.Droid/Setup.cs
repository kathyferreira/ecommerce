﻿using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Forms.Droid.Platform;
using MvvmCross.Forms.Platform;
using MvvmCross.Platform.Logging;
using MvvmCross.Plugins.Json;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ECommerce.Droid
{
    /// <summary>
    /// Platform setup class
    /// </summary>
    public class Setup : MvxFormsAndroidSetup
    {
        /// <summary>
        /// Constructor
        /// </summary>        
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        /// <summary>
        /// Sets the log provider
        /// </summary>
        /// <returns></returns>
        protected override MvxLogProviderType GetDefaultLogProviderType()
            => MvxLogProviderType.None;

        /// <summary>
        /// Returns the assemblies used
        /// </summary>
        protected override IEnumerable<Assembly> GetViewAssemblies()
        {
            return new List<Assembly>(base.GetViewAssemblies().Union(new[] { typeof(ECommerce.Core.App).GetTypeInfo().Assembly }));
        }

        /// <summary>
        ///  Creates the app
        /// </summary>
        /// <returns></returns>
        protected override MvxFormsApplication CreateFormsApplication() => new ECommerce.Core.App();

        /// <summary>
        ///  Creates the Forms app
        /// </summary>
        /// <returns></returns>
        protected override IMvxApplication CreateApp() => new ECommerce.Core.CoreApp();

        /// <summary>
        /// Ensures all the plugins are loaded
        /// </summary>
        protected override void PerformBootstrapActions()
        {
            base.PerformBootstrapActions();
            PluginLoader.Instance.EnsureLoaded();
        }
    }
}