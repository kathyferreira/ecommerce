﻿using MvvmCross.Platform.Plugins;

namespace ECommerce.Droid.Bootstrap
{
    public class ResourceLoaderPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.ResourceLoader.PluginLoader>
    {
    }
}