﻿using MvvmCross.Platform;
using MvvmCross.Platform.Droid.Platform;
using MvvmCross.Platform.Plugins;
using Plugin.Fingerprint;

namespace ECommerce.Droid.Bootstrap
{
    /// <summary>
    /// Logic for loading Fingerprint Plugin
    /// </summary>
    public class FingerprintPlugin : IMvxPlugin
    {
        /// <summary>
        /// Loads the fingerprint plugin
        /// </summary>
        public void Load()
        {
            CrossFingerprint.SetCurrentActivityResolver(() => Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity);
            Mvx.LazyConstructAndRegisterSingleton(() => CrossFingerprint.Current);
        }
    }
}