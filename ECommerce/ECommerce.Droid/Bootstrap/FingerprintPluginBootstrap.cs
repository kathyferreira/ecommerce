﻿using MvvmCross.Platform.Plugins;

namespace ECommerce.Droid.Bootstrap
{
    /// <summary>
    /// Bootstraps the Finger Print plugin
    /// </summary>
    public class FingerprintPluginBootstrap 
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Fingerprint.PluginLoader>
    {
    }
}