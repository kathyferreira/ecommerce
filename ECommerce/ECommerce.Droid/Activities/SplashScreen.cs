﻿
using Android.App;
using Android.Content.PM;
using MvvmCross.Droid.Views;

namespace ECommerce.Droid.Activities
{
    /// <summary>
    /// Android splashscreen
    /// </summary>
    [Activity(
        Label = "@string/ApplicationName"
        , MainLauncher = true
        , Icon = "@drawable/icon"
        , Theme = "@style/Theme.Splash"
        , NoHistory = true
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {

        /// <summary>
        /// Constructor
        /// Defines the resource to use as splash
        /// </summary>
        public SplashScreen() : base(Resource.Layout.SplashScreen)
        {
        }

        /// <summary>
        /// Called the first time the activity is created
        /// </summary>        
        protected override void TriggerFirstNavigate()
        {
            StartActivity(typeof(MainActivity));
            base.TriggerFirstNavigate();

        }
    }
}