﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using ECommerce.Droid.Dialogs;
using MvvmCross.Forms.Droid.Views;
using Plugin.Fingerprint;

namespace ECommerce.Droid
{
    /// <summary>
    /// Main activity 
    /// </summary>
    [Activity(
        Label = "@string/ApplicationName",
        Icon = "@drawable/icon",
        Theme = "@style/MainTheme",
        ScreenOrientation = ScreenOrientation.Portrait,
        LaunchMode = LaunchMode.SingleTask,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : MvxFormsAppCompatActivity<Core.ViewModels.StartUpViewModel>
    {
        /// <summary>
        /// Setups forms
        /// </summary>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            CrossFingerprint.SetDialogFragmentType<CustomFingerprintDialogFragment>();
        }

        /// <summary>
        /// Handles the back pressed
        /// </summary>
        public override void OnBackPressed()
        {
            MoveTaskToBack(false);
        }
    }

    ///// <summary>
    ///// Overrides the initialize to use the gorilla player for design
    ///// </summary>
    ///// <param name="bundle"></param>
    //public override void InitializeForms(Bundle bundle)
    //{
    //    if (FormsApplication.MainPage != null)
    //    {
    //        global::Xamarin.Forms.Forms.Init(this, bundle, GetResourceAssembly());

    //        var gorillaConfig = new UXDivers.Gorilla.Config("Good Gorilla").RegisterAssembliesFromTypes<
    //            ReferralsApp.Core.App,
    //            MvvmCross.Forms.Bindings.MvxBindingCreator>();
    //        LoadApplication(UXDivers.Gorilla.Droid.Player.CreateApplication(this, gorillaConfig));
    //    }
    //}
}

